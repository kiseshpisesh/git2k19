﻿/**
*@file  Practice_2019.cpp
*@author Кулибаба В.Э., гр. 515а
*@date 21 июня 2019
*@brief Учебная Практика
*
*Проверка знаний. Задание
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <conio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <Windows.h>

/*
* @param avtor - имя автора
* @param nazvanie - Название книги
* @param tiraj - тираж книги
* @param price - цена книги
*/

typedef struct {
	char avtor[50];
	char nazvanie[30];
	int tiraj;
	int price;
} KNIGA;

int main()
{
	setlocale(LC_ALL, "Rus");
	SetConsoleCP(1251); SetConsoleOutputCP(1251);
	FILE *ukazatel;
	KNIGA massiv[12] = { 0 };
	ukazatel = (FILE*)fopen("book_file.txt", "r");
	int res = 0, key = 0, value = 0;
	fseek(ukazatel, 0, SEEK_SET);
	for (int i = 0; i < 12; i++) {
		fgets(massiv[i].avtor, 50, ukazatel);
		fgets(massiv[i].nazvanie, 30, ukazatel);
		fscanf(ukazatel, "%d", &massiv[i].tiraj);
		fscanf(ukazatel, "%d;\n", &massiv[i].price);
	}
	printf(" Меню\n 1. Просмотр\n 2. Поиск\n");
	scanf(" %d", &key);
	switch (key)
	{
	case 1: for (int i = 0; i < 12; i++) {
		puts(massiv[i].avtor); puts(massiv[i].nazvanie);
		printf("%d\n\n", massiv[i].tiraj); printf("%d;\n\n\n", massiv[i].price);
	} break;
	case 2: for (int i = 0; i < 12; i++)
	{
		if (strstr(massiv[i].nazvanie, "Язык С") != NULL) {
			puts(massiv[i].avtor);
			puts(massiv[i].nazvanie);
			printf("%d\n\n", massiv[i].tiraj); printf("%d;\n\n\n", massiv[i].price);
			value += massiv[i].tiraj;
			res += massiv[i].tiraj * massiv[i].price;
		}
	}
			printf("Количество %d\n Общая стоимость %d\n", value, res);
			break;
	default: printf("Ошибка"); break;
	}
	fclose(ukazatel); _getch();
	return 0;
}